# ✂️  Snippets


👋 Aloha! 

You _probably_ want [the snippets](https://gitlab.com/bcarranza/snippets/-/snippets) attached to this project **or** the [snippets](https://gitlab.com/users/bcarranza/snippets) tied to my user (@bcarranza). There's not much else here. 


--
Brie 🦄

